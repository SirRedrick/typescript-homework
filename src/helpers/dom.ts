interface IElement {
    tagName: string;
    className?: string;
    attributes?: { [key: string]: string };
    nameSpace?: string;
    children?: (Element | string)[];
    eventListeners?: Array<[eventType: string, callback: () => void]>;
}

const createElement = ({
    tagName,
    className,
    attributes,
    nameSpace,
    children,
    eventListeners,
}: IElement): Element => {
    const element = nameSpace
        ? document.createElementNS(nameSpace, tagName)
        : document.createElement(tagName);

    if (className) {
        addClasses(element, className);
    }

    if (attributes) {
        addAttributes(element, attributes);
    }

    if (children) {
        element.append(...children);
    }

    if (eventListeners) {
        addEventListeners(element, eventListeners);
    }

    return element;
};

const addClasses = (element: Element, className: string): void => {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
};

const addAttributes = (
    element: Element,
    attributes: { [key: string]: string }
) => {
    Object.keys(attributes).forEach((key) =>
        element.setAttribute(key, attributes[key])
    );
};

const addEventListeners = (
    element: Element,
    eventListeners: Array<[eventType: string, callback: () => void]>
) => {
    eventListeners.forEach(([eventType, callback]) =>
        element.addEventListener(eventType, callback)
    );
};

export { createElement };
