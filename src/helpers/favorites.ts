const addToFavorites = (id: number): void => {
    const favorites = getFavorites();
    favorites.push(id);
    localStorage.setItem('favorites', JSON.stringify(favorites));
};

const removeFromFavorites = (id: number): void => {
    const favorites = getFavorites();
    localStorage.setItem(
        'favorites',
        JSON.stringify(favorites.filter((favorite: number) => favorite !== id))
    );
};

const checkIfFavorite = (id: number): boolean => {
    const favorites = getFavorites();
    return favorites.includes(id);
};

const getFavorites = (): number[] => {
    const items = localStorage.getItem('favorites');
    return items ? JSON.parse(items) : [];
};

export { getFavorites, addToFavorites, removeFromFavorites, checkIfFavorite };
