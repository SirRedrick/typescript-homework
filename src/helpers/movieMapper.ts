import { checkIfFavorite } from './favorites';
import { IMovie } from '../interfaces/IMovie';

type RawFilm = {
    adult: boolean;
    backdrop_path: string;
    genre_ids: number[];
    id: number;
    original_language: string;
    original_title: string;
    overview: string;
    popularity: number;
    poster_path: string;
    release_date: string;
    title: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
};

const movieMapper = ({
    id,
    overview,
    poster_path,
    release_date,
    backdrop_path,
    title,
}: RawFilm): IMovie => {
    return {
        id,
        overview,
        poster_path,
        release_date,
        backdrop_path,
        title,
        isFavorite: checkIfFavorite(id),
    };
};

export { movieMapper };
