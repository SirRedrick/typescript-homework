import { App } from './components/App';

export async function render(): Promise<void> {
    new App();
}
