interface IMovie {
    id: number;
    overview: string;
    poster_path: string;
    release_date: string;
    backdrop_path: string;
    title: string;
    isFavorite: boolean;
}

export { IMovie };
