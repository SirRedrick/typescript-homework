const apiBaseUrl = 'https://api.themoviedb.org/3';
const imgBaseUrl = 'https://image.tmdb.org/t/p/original';

const searchPath = '/search/movie';
const popularPath = '/movie/popular';
const upcomingPath = '/movie/upcoming';
const topRatedPath = '/movie/top_rated';

export {
    apiBaseUrl,
    imgBaseUrl,
    searchPath,
    popularPath,
    upcomingPath,
    topRatedPath,
};
