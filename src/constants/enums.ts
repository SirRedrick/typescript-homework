enum Filter {
    Search,
    Popular,
    Upcoming,
    TopRated,
}

export { Filter };
