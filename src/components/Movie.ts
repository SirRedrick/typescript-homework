import { createElement } from '../helpers/dom';
import { IMovie } from '../interfaces/IMovie';
import { imgBaseUrl } from '../constants/api';

const createMovie = (
    { id, overview, poster_path, release_date, isFavorite }: IMovie,
    eventListener: () => void
): HTMLElement => {
    const card = createCard(
        id,
        overview,
        poster_path,
        release_date,
        isFavorite,
        eventListener
    );

    return createElement({
        tagName: 'div',
        className: 'col-lg-3 col-md-4 col-12 p-2',
        children: [card],
    }) as HTMLElement;
};

const createFavoriteMovie = (
    { id, overview, poster_path, release_date }: IMovie,
    eventListener: () => void
): HTMLElement => {
    const card = createCard(
        id,
        overview,
        poster_path,
        release_date,
        true,
        eventListener
    );

    return createElement({
        tagName: 'div',
        className: 'col-12 p-2',
        children: [card],
    }) as HTMLElement;
};

const createCard = (
    id: number,
    overview: string,
    poster_path: string,
    release_date: string,
    isFavorite: boolean,
    eventListener: () => void
): HTMLElement => {
    const heartSVG = createHeartSVG(isFavorite, eventListener);
    const poster = createPoster(poster_path);
    const body = createBody(overview, release_date);

    return createElement({
        tagName: 'div',
        className: 'card shadow-sm',
        attributes: {
            id: id.toString(),
        },
        children: [poster, heartSVG, body],
    }) as HTMLElement;
};

const createPoster = (poster_path: string): HTMLElement => {
    return poster_path === null
        ? createEmptyImagePlaceholder()
        : (createElement({
              tagName: 'img',
              attributes: {
                  src: imgBaseUrl + poster_path,
              },
          }) as HTMLElement);
};

const createBody = (overview: string, release_date: string): HTMLElement => {
    const text = createElement({
        tagName: 'p',
        className: 'card-text truncate',
        children: [overview],
    });
    const releaseDate = createElement({
        tagName: 'small',
        className: 'text-muted',
        children: [release_date],
    });
    const releaseDateContainer = createElement({
        tagName: 'div',
        className: 'd-flex justify-content-between align-items-center',
        children: [releaseDate],
    });

    return createElement({
        tagName: 'div',
        className: 'card-body',
        children: [text, releaseDateContainer],
    }) as HTMLElement;
};

const createEmptyImagePlaceholder = (): HTMLElement => {
    const noImageSVG = createNoImageSVG();
    return createElement({
        tagName: 'div',
        className: 'd-flex justify-content-center align-items-center',
        attributes: {
            style: 'aspect-ratio: 2 / 3',
        },
        children: [noImageSVG],
    }) as HTMLElement;
};

const createHeartSVG = (isFavorite: boolean, eventListener: () => void) => {
    const path = createElement({
        tagName: 'path',
        attributes: {
            'fill-rule': 'evenodd',
            d: 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z',
        },
        nameSpace: 'http://www.w3.org/2000/svg',
    });

    return createElement({
        tagName: 'svg',
        className: 'bi bi-heart-fill position-absolute p-2',
        attributes: {
            stroke: 'red',
            fill: isFavorite ? 'red' : '#ff000078',
            width: '50',
            height: '50',
            viewBox: '0 -2 18 22',
        },
        nameSpace: 'http://www.w3.org/2000/svg',
        children: [path],
        eventListeners: [['click', eventListener]],
    });
};

const createNoImageSVG = () => {
    const path1 = createElement({
        tagName: 'path',
        attributes: {
            d: 'M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z',
        },
        nameSpace: 'http://www.w3.org/2000/svg',
    });
    const path2 = createElement({
        tagName: 'path',
        attributes: {
            d: 'M1.5 2A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13zm13 1a.5.5 0 0 1 .5.5v6l-3.775-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12v.54A.505.505 0 0 1 1 12.5v-9a.5.5 0 0 1 .5-.5h13z',
        },
        nameSpace: 'http://www.w3.org/2000/svg',
    });

    return createElement({
        tagName: 'svg',
        className: 'bi bi-card-image',
        attributes: {
            width: '50%',
            fill: 'currentColor',
            viewBox: '0 0 16 16',
        },
        nameSpace: 'http://www.w3.org/2000/svg',
        children: [path1, path2],
    });
};

export { createMovie, createFavoriteMovie };
