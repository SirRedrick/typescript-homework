import { http } from '../services/http';
import {
    getFavorites,
    checkIfFavorite,
    addToFavorites,
    removeFromFavorites,
} from '../helpers/favorites';
import { IMovie } from '../interfaces/IMovie';
import { createMovie, createFavoriteMovie } from './Movie';
import { imgBaseUrl } from '../constants/api';
import { Filter } from '../constants/enums';

class App {
    private readonly filmContainer: HTMLElement;
    private readonly favoriteContainer: HTMLElement;
    private readonly cover: {
        container: HTMLElement;
        title: HTMLElement;
        overview: HTMLElement;
    };
    private readonly search: HTMLInputElement;
    private readonly buttons: {
        search: HTMLElement;
        popular: HTMLElement;
        upcoming: HTMLElement;
        topRated: HTMLElement;
        loadMore: HTMLElement;
    };

    private movies: IMovie[];
    private favoriteMovies: IMovie[];
    private favoriteIds: number[];
    private currentFilter: Filter;
    private currentPage: number;
    private currentQuery: string;

    constructor() {
        this.filmContainer = document.getElementById(
            'film-container'
        ) as HTMLElement;
        this.favoriteContainer = document.getElementById(
            'favorite-movies'
        ) as HTMLElement;
        this.cover = {
            container: document.getElementById('random-movie') as HTMLElement,
            title: document.getElementById('random-movie-name') as HTMLElement,
            overview: document.getElementById(
                'random-movie-description'
            ) as HTMLElement,
        };
        this.search = document.getElementById('search') as HTMLInputElement;
        this.buttons = {
            search: document.getElementById('submit') as HTMLElement,
            popular: document.getElementById('popular') as HTMLElement,
            upcoming: document.getElementById('upcoming') as HTMLElement,
            topRated: document.getElementById('top_rated') as HTMLElement,
            loadMore: document.getElementById('load-more') as HTMLElement,
        };

        this.toggleFavorite = this.toggleFavorite.bind(this);
        this.searchMovie = this.searchMovie.bind(this);
        this.getPopular = this.getPopular.bind(this);
        this.getUpcoming = this.getUpcoming.bind(this);
        this.getTopRated = this.getTopRated.bind(this);
        this.loadMore = this.loadMore.bind(this);

        this.buttons.search.addEventListener('click', this.searchMovie);
        this.buttons.popular.addEventListener('click', this.getPopular);
        this.buttons.upcoming.addEventListener('click', this.getUpcoming);
        this.buttons.topRated.addEventListener('click', this.getTopRated);
        this.buttons.loadMore.addEventListener('click', this.loadMore);

        this.movies = [];
        this.favoriteMovies = [];
        this.favoriteIds = getFavorites();
        this.currentFilter = Filter.Popular;
        this.currentPage = 1;
        this.currentQuery = '';

        this.init();
    }

    async init(): Promise<void> {
        this.movies = await http.getPopular();
        this.favoriteMovies = await http.getFavorites(this.favoriteIds);

        this.renderCover();
        this.renderMovies();
        this.renderFavorites();
    }

    renderMovies(): void {
        const movieElements = this.movies.map((movie) =>
            createMovie(movie, () => this.toggleFavorite(movie.id))
        );

        this.filmContainer.innerHTML = '';
        this.filmContainer.append(...movieElements);
    }

    renderFavorites(): void {
        const favoriteElements = this.favoriteMovies.map((movie) =>
            createFavoriteMovie(movie, () => this.toggleFavorite(movie.id))
        );

        this.favoriteContainer.innerText = '';
        this.favoriteContainer.append(...favoriteElements);
    }

    renderCover(): void {
        const { backdrop_path, title, overview } = this.getRandomMovie();

        this.cover.container.style.backgroundImage = `url(${imgBaseUrl}${backdrop_path})`;
        this.cover.title.innerText = title;
        this.cover.overview.innerText = overview;
    }

    async searchMovie(): Promise<void> {
        const query = this.search.value;
        if (query === '') return;

        this.currentFilter = Filter.Search;
        this.currentPage = 1;
        this.currentQuery = query;

        this.movies = await http.search(query);

        this.renderMovies();
    }

    async getPopular(): Promise<void> {
        this.currentFilter = Filter.Popular;
        this.currentPage = 1;

        this.movies = await http.getPopular();

        this.renderMovies();
    }

    async getUpcoming(): Promise<void> {
        this.currentFilter = Filter.Upcoming;
        this.currentPage = 1;

        this.movies = await http.getUpcoming();

        this.renderMovies();
    }

    async getTopRated(): Promise<void> {
        this.currentFilter = Filter.TopRated;
        this.currentPage = 1;

        this.movies = await http.getTopRated();

        this.renderMovies();
    }

    async loadMore(): Promise<void> {
        this.currentPage++;
        let newFilms;

        switch (this.currentFilter) {
            case Filter.Search:
                newFilms = await http.search(
                    this.currentQuery,
                    this.currentPage
                );
                break;
            case Filter.Upcoming:
                newFilms = await http.getUpcoming(this.currentPage);
                break;
            case Filter.Popular:
                newFilms = await http.getPopular(this.currentPage);
                break;
            case Filter.TopRated:
                newFilms = await http.getTopRated(this.currentPage);
                break;
        }

        this.movies = this.movies.concat(newFilms);

        this.renderMovies();
    }

    async toggleFavorite(id: number): Promise<void> {
        if (checkIfFavorite(id)) {
            removeFromFavorites(id);
            this.favoriteIds = this.favoriteIds.filter(
                (favoriteId) => favoriteId !== id
            );
            this.favoriteMovies = this.favoriteMovies.filter(
                (movie) => movie.id !== id
            );
        } else {
            addToFavorites(id);
            const result = await http.getFavorite(id);
            this.favoriteIds = this.favoriteIds.concat(id);
            this.favoriteMovies = this.favoriteMovies.concat(result);
        }

        this.movies.map((movie) => {
            movie.isFavorite =
                movie.id === id ? !movie.isFavorite : movie.isFavorite;
            return movie;
        });
        this.renderMovies();
        this.renderFavorites();
    }

    getRandomMovie(): IMovie {
        return this.movies[Math.floor(Math.random() * this.movies.length)];
    }
}

export { App };
