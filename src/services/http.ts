import {
    apiBaseUrl,
    popularPath,
    searchPath,
    upcomingPath,
    topRatedPath,
} from '../constants/api';
import { movieMapper } from '../helpers/movieMapper';
import { IMovie } from '../interfaces/IMovie';

const http = {
    search: async (query: string, page = 1): Promise<IMovie[]> =>
        fetch(
            `${apiBaseUrl}${searchPath}?api_key=${process.env.API_KEY}&query=${query}&page=${page}`
        )
            .then((res) => res.json())
            .then((data) => data.results.map(movieMapper)),
    getPopular: async (page = 1): Promise<IMovie[]> =>
        fetch(
            `${apiBaseUrl}${popularPath}?api_key=${process.env.API_KEY}&page=${page}`
        )
            .then((res) => res.json())
            .then((data) => data.results.map(movieMapper)),
    getUpcoming: async (page = 1): Promise<IMovie[]> =>
        fetch(
            `${apiBaseUrl}${upcomingPath}?api_key=${process.env.API_KEY}&page=${page}`
        )
            .then((res) => res.json())
            .then((data) => data.results.map(movieMapper)),
    getTopRated: async (page = 1): Promise<IMovie[]> =>
        fetch(
            `${apiBaseUrl}${topRatedPath}?api_key=${process.env.API_KEY}&page=${page}`
        )
            .then((res) => res.json())
            .then((data) => data.results.map(movieMapper)),
    getFavorites: async (favorites: number[]): Promise<IMovie[] | []> => {
        const results = favorites.map(
            async (favorite: number) =>
                await fetch(
                    `${apiBaseUrl}/movie/${favorite}?api_key=${process.env.API_KEY}`
                ).then((res) => res.json())
        );

        return Promise.all(results);
    },
    getFavorite: async (id: number): Promise<IMovie> =>
        fetch(`${apiBaseUrl}/movie/${id}?api_key=${process.env.API_KEY}`)
            .then((res) => res.json())
            .then((data) => [data].map(movieMapper)[0]),
};

export { http };
